# dbnary-sparql-tutorials

A set of tutorials on querying DBnary using the public SPARQL endpoint

## Folder jupyter-python

This folder contains the docker-compose.yml + a notebook to start the tutorial session.

It is to be used if you installed docker on your machine. For this, open a shell session (using a terminal app) and:

- change directory to the jupyter-sparql folder: `cd jupyter-sparql`
- launch docker app and launch the container: `docker-compose up`
- copy the localhost URL that should appear after container loading (with the token) and paste it in your browser
- you can then open the notebook or create a new one

## Folder sparql

This folder contains some queries to start the tutorial session.

It is to be used if you use Visual Studio Code or a browser to the DBnary endpoint.

Just open each query and execute it (or copy paste it on the DBnary endpoint).